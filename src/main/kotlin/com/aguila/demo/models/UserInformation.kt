package com.aguila.demo.models

import com.fasterxml.jackson.annotation.JsonProperty

class UserInformation {

    @JsonProperty("first_name")
    var firstName: String = ""

    @JsonProperty("last_name")
    var lastName: String = ""
}