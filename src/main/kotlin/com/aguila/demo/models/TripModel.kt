package com.aguila.demo.models

import com.fasterxml.jackson.annotation.JsonProperty
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document
class TripModel {
    @Id
    var id: String? = null

    var start: PickupInformation = PickupInformation()

    var end: PickupInformation? = null

    var passenger: UserInformation = UserInformation()

    var driver: UserInformation = UserInformation()

    var trip: CarInformation? = null

    var status: String? = ""

    var country: GeoData = GeoData()

    var city: GeoData = GeoData()

    @JsonProperty("check_code")
    var checkCode: String? = ""

    @JsonProperty("created_at")
    var createdAt: DateFormat? = null

    @JsonProperty("updated_at")
    var updatedAt: DateFormat? = null

    @JsonProperty("price")
    var price: Double = 0.0

    @JsonProperty("driver_location")
    var driverLocation: PickupLocation? = null
}

