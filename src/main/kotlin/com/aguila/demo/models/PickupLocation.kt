package com.aguila.demo.models

class PickupLocation {

    var type: String = ""

    var coordinates: MutableList<Double> = mutableListOf()
}