package com.aguila.demo.models

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonProperty
import java.time.LocalDateTime

class DateFormat {
    @JsonProperty("\$date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    var date: LocalDateTime? = LocalDateTime.now()
}