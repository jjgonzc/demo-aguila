package com.aguila.demo.models

import com.fasterxml.jackson.annotation.JsonProperty

class PickupInformation {
    var date: DateFormat? = null

    @JsonProperty("pickup_address")
    var pickupAddress: String = ""

    @JsonProperty("pickup_location")
    var pickupLocation: PickupLocation? = null
}