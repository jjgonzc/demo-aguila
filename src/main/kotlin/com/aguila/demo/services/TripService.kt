package com.aguila.demo.services

import com.aguila.demo.models.GeoData
import com.aguila.demo.models.TripModel
import com.aguila.demo.repositories.TripsRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class TripService {

    @Autowired
    private lateinit var tripsRepository: TripsRepository

    fun post(tripModel: TripModel): TripModel {

        return this.tripsRepository.save(tripModel)
    }

    fun getAllTrips(): MutableList<TripModel> {

        return this.tripsRepository.findAll()
    }

    fun getAllTripsByCity(geoData: GeoData): MutableList<TripModel> {

        return this.tripsRepository.findAllByCity(geoData)
    }

    @Throws(Exception::class)
    fun updateTrip(tripModel: TripModel): TripModel? {
        var trip = TripModel()

        tripModel.id?.let {
            if (this.tripsRepository.existsById(it)) {
                trip = this.tripsRepository.save(tripModel)
            } else {
                throw object : Exception("Trip not found") {}
            }
        }

        return trip
    }
}