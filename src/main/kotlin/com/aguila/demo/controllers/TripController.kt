package com.aguila.demo.controllers

import com.aguila.demo.models.GeoData
import com.aguila.demo.models.TripModel
import com.aguila.demo.routers.Router
import com.aguila.demo.services.TripService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@RequestMapping(value = [(Router.TRIP)], produces = [(MediaType.APPLICATION_JSON_UTF8_VALUE)])
class TripController {

    @Autowired
    private lateinit var tripService: TripService

    @GetMapping("/all")
    fun getAllTrips(): MutableList<TripModel> {

        return this.tripService.getAllTrips()
    }

    @PostMapping("get/city")
    fun getAllTripsByCity(@Valid @RequestBody geoData: GeoData): MutableList<TripModel> {

        return this.tripService.getAllTripsByCity(geoData)
    }

    @PutMapping
    fun updateTrip(@Valid @RequestBody tripModel: TripModel): TripModel? {
        return this.tripService.updateTrip(tripModel)
    }

    @PostMapping
    fun createTrip(@Valid @RequestBody tripModel: TripModel): TripModel {

        return this.tripService.post(tripModel)
    }
}