package com.aguila.demo.routers

object Router {
    private const val BASE_ROUTER = "/api/ms/demo"
    const val TRIP = "$BASE_ROUTER/trip"
}