package com.aguila.demo.repositories

import com.aguila.demo.models.GeoData
import com.aguila.demo.models.TripModel
import org.springframework.data.mongodb.repository.MongoRepository

interface TripsRepository : MongoRepository<TripModel, String> {
    fun findAllByCity(geoData: GeoData): MutableList<TripModel>
}