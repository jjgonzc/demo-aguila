demo-aguila

##Requirements

- Spring Framework & Spring Boot 2
- Gradle
- MongoDB

## Build

1. Test your changes
```
$ ./gradlew test 
```

2. Build the project without Test
```
$ ./gradlew -x test build 
```

3. Build the project complete
```
$ ./gradlew build 
```
## USAGE

```
Create trip
/api/ms/demo/trip
POST

Request

{
  "start": {
    "date": {
      "$date": "2019-09-08 21:17:13"
    },
    "pickup_address": "Cl. 90 #19-41, Bogotá, Colombia",
    "pickup_location": {
      "type": "Point",
      "coordinates": [
        -74.0565192,
        4.6761959
      ]
    }
  },
  "end": {
    "date": null,
    "pickup_address": "Ac. 100 #13-21, Bogotá, Colombia",
    "pickup_location": {
      "type": "Point",
      "coordinates": [
        -74.0465655,
        4.6830892
      ]
    }
  },
  "country": {
    "name": "Colombia"
  },
  "city": {
    "name": "Demo2"
  },
  "passenger": {
    "first_name": "Ricardo",
    "last_name": "Sarmiento"
  },
  "driver": {
    "first_name": "Julio Alberto",
    "last_name": "Mesa Rodriguez"
  },
  "car": {
    "plate": "ESM308"
  },
  "status": "started",
  "check_code": "66",
  "createdAt": {
    "$date": "2019-09-08 21:17:13"
  },
  "updatedAt": {
    "$date": "2019-09-08 21:17:13"
  },
  "price": 13800,
  "driver_location": {
    "type": "Point",
    "coordinates": [
      -74.06017631292343,
      4.669553302335373
    ]
  }
}

Update trip
/api/ms/demo/trip
PUT

Request
{
        "id": "5d76f764e1345a54e7f29a5e",
        "start": {
            "date": {
                "$date": "2019-09-08 21:17:13"
            },
            "pickup_address": "Cl. 90 #19-41, Bogotá, Colombia",
            "pickup_location": {
                "type": "Point",
                "coordinates": [
                    -74.0565192,
                    5.6761959
                ]
            }
        },
        "end": {
            "date": null,
            "pickup_address": "Ac. 100 #13-21, Bogotá, Colombia",
            "pickup_location": {
                "type": "Point",
                "coordinates": [
                    -74.0465655,
                    4.6830892
                ]
            }
        },
        "passenger": {
            "first_name": "Ricardo",
            "last_name": "Sarmiento"
        },
        "driver": {
            "first_name": "Julio Alberto",
            "last_name": "Mesa Rodriguez"
        },
        "trip": null,
        "status": "started",
        "country": {
            "name": "Colombia"
        },
        "city": {
            "name": "Demo2"
        },
        "check_code": "66",
        "created_at": null,
        "updated_at": null,
        "price": 13800,
        "driver_location": {
            "type": "Point",
            "coordinates": [
                -74.06017631292343,
                4.669553302335373
            ]
        }
    }

Get all trips
/api/ms/demo/trip/all
GET

Get trips by city
/api/ms/demo/trip/get/city
POST

Request

{
    "name": "Medellin"
}

```

Configurar credenciales de Mongodb en application.properties

